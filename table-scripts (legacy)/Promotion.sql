CREATE TABLE Promotion
(
    PromoCode char(10),
    StartDate datetime NOT NULL,
    EndDate datetime NOT NULL,
    PromoRate smallmoney NOT NULL,
    PromoDescription varchar(200),
    CONSTRAINT PK_Promotion_PromoCode PRIMARY KEY (PromoCode)
)