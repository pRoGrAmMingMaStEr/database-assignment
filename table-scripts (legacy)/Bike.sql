CREATE TABLE Bike 
(
    BikeID char(10) NOT NULL,
    BikeStopID char(5) NOT NULL, 
    Rate smallmoney NOT NULL,
    Status varchar(50) NOT NULL,
    CONSTRAINT PK_Bike PRIMARY KEY (BikeID),
    CONSTRAINT FK_Bike_BikeStopID FOREIGN KEY (BikeStopID)
        REFERENCES BikeStop(BikeStopID),
    CONSTRAINT CHK_Bike_Status CHECK (Status in ('Damaged', 'Under Repair', 'Available'))
)