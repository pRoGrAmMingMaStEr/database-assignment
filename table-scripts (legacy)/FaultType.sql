CREATE TABLE FaultType
(
    FaultCode char(10) NOT NULL,
    FaultDesc text NOT NULL,
    CONSTRAINT PK_FaultType PRIMARY KEY (FaultCode)
)