CREATE TABLE MaintType 
(
    TypeCode char(10),
    TypeDesc char(100) NOT NULL,
    CONSTRAINT PK_MaintType_TypeCode PRIMARY KEY (TypeCode)
)