CREATE TABLE Comment
(
    FBID char(10) NOT NULL,
    Message text NOT NULL,
    CONSTRAINT PK_Comment_FBID PRIMARY KEY (FBID),
    CONSTRAINT FK_Comment_FBID FOREIGN KEY (FBID) 
        REFERENCES Feedback(FBID)
)