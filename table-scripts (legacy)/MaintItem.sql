CREATE TABLE MaintItem
(
  MaintID char(10) NOT NULL,
  SNo char(10) NOT NULL,
  TypeCode char(10) NOT NULL,
  Desc_ text,
  Constraint PK_MaintItem PRIMARY KEY(MaintID, SNo),
  Constraint FK_MaintItem_TypeCode FOREIGN KEY(TypeCode)
    REFERENCES MaintType(TypeCode),
  Constraint FK_MaintItem_MaintID FOREIGN KEY(MaintID)
    REFERENCES Maintenance(MaintID)
)