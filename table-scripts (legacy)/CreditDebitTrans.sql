CREATE TABLE CreditDebitTrans
(
    TransID char(10) PRIMARY KEY,
    TransAmt smallmoney NOT NULL,
    TransType varchar(10) NOT NULL,
    TransDateTime datetime NOT NULL,
    CardNo char(12) NOT NULL,
    RideID char(10) NOT NULL,
    CONSTRAINT FK_CreditDebitTrans_CardNo
        FOREIGN KEY(CardNo)REFERENCES CreditCard(CardNo),
    CONSTRAINT FK_CreditDebitTrans_RideID
        FOREIGN KEY(RideID)REFERENCES Ride(RideID),
    CONSTRAINT CHK_CreditDebitTrans_TransType
        CHECK (TransType IN ('Debit', 'Top up'))
)