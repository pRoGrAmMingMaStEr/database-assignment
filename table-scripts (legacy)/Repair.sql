CREATE TABLE Repair
(
    MaintID char(10) NOT NULL,
    CONSTRAINT PK_Repair_MaintID PRIMARY KEY (MaintID),
    CONSTRAINT FK_Repair_MaintID FOREIGN KEY (MaintID) 
        REFERENCES Maintenance(MaintID)
)