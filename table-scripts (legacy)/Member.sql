CREATE TABLE Member
( 
    MemberID char(10),
    MemberName varchar(50) NOT NULL,
    Email varchar(100) NOT NULL,
    Phone char(8) NOT NULL,
    RewardPts int NULL,
    Deposit smallmoney DEFAULT 0 NOT NULL,
    CreditBalance smallmoney DEFAULT 0 NOT NULL,
    CONSTRAINT PK_Member_MemberID PRIMARY KEY (MemberID)
)