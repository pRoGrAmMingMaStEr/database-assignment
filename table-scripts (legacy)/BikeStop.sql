CREATE TABLE BikeStop
(
    StopLat decimal NOT NULL,
    StopLong decimal NOT NULL,
    StopRadius decimal NOT NULL,
    BikeStopID char(5),
    CONSTRAINT PK_BikeStop PRIMARY KEY (BikeStopID),
    CONSTRAINT CHK_BikeStop_StopRadius CHECK(StopRadius BETWEEN 2 AND 25)
)