CREATE TABLE Fault
(
    FBID char(10) NOT NULL,
    MaintID char(10) NOT NULL,
    FaultCode char(10) NOT NULL,
    BikeID char(10) NOT NULL,
    CONSTRAINT FK_Fault_BikeID FOREIGN KEY (BikeID) 
        REFERENCES Bike(BikeID),
    CONSTRAINT FK_Fault_FBID 
    FOREIGN KEY (FBID) REFERENCES Feedback(FBID),
    CONSTRAINT FK_Fault_FaultType 
    FOREIGN KEY (FaultCode) REFERENCES FaultType(FaultCode),
    CONSTRAINT FK_Fault_MaintID
    FOREIGN KEY (MaintID) REFERENCES Maintenance(MaintID)
)