CREATE TABLE Reservation
(
  MemberID char(10) NOT NULL ,
  BikeID char(10) NOT NULL,
  ResStatus varchar(50) NOT NULL,
  DateTimeRes datetime NOT NULL,
  DateTimeReservedFor datetime NOT NULL,
  CONSTRAINT PK_Reservation PRIMARY KEY (MemberID,BikeID),
  CONSTRAINT FK_Reservation_MemberID 
	FOREIGN KEY (MemberID) REFERENCES Member(MemberID),
  CONSTRAINT FK_Reservation_BikeID 
	FOREIGN KEY (BikeID) REFERENCES Bike(BikeID),
  CONSTRAINT CHK_ResStatus CHECK(ResStatus IN ('Confirmed', 'Cancelled', 'NoShow')),
  CONSTRAINT CHK_Reservation_DateTime CHECK(
    DATEDIFF(hour, DateTimeRes, DateTimeReservedFor) <= 2 
    AND DateTimeRes < DateTimeReservedFor)
)