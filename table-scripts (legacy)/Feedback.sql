CREATE TABLE Feedback
(
    FBID char(10) NOT NULL,
    FBDateTime datetime NOT NULL,
    MemberID char(10) NOT NULL,
    CONSTRAINT FK_MemberID FOREIGN KEY (MemberID)
        REFERENCES Member(MemberID),
    CONSTRAINT PK_Feedback PRIMARY KEY(FBID)
)