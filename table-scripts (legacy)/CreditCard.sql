CREATE TABLE CreditCard(
    CardNo char(12) NOT NULL,
    CardType varchar(20) NOT NULL,
    ExpiryDate datetime NOT NULL,
    CVV Char(3) NOT NULL,
    MemberID char(10) NOT NULL,
    CONSTRAINT PK_CreditCard PRIMARY KEY(CardNo),
    CONSTRAINT FK_CreditCard_MemberID
        FOREIGN KEY(MemberID)REFERENCES Member(MemberID),
    CONSTRAINT CHK_CC_ExpiryDate CHECK (ExpiryDate > GETDATE()),
    CONSTRAINT CHK_CC_CardType CHECK (CardType IN ('Visa', 'Master', 'Diners'))
)