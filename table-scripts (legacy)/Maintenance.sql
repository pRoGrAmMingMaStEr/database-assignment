CREATE TABLE Maintenance
(
    MaintID char(10),
    MaintDate datetime NOT NULL,
    BikeID char(10) NOT NULL,
    CONSTRAINT PK_Maintenance_MaintID PRIMARY KEY (MaintID),
    CONSTRAINT FK_Maintenance_BikeID
    FOREIGN KEY (BikeID) REFERENCES Bike(BikeID)
)