CREATE TABLE Ride
(
    RideID char(10) NOT NULL,
    MemberID char(10) NOT NULL,
    BikeID char(10) NOT NULL,
    BikeStopID char(5) NOT NULL,
    PromoCode char(10) NULL,
    DateTimeStart datetime NOT NULL,
    DateTimeEnd  datetime NOT NULL,
    PtsRedeemed tinyint,
    RidePts tinyint,
    Distance float NOT NULL,
    Cost smallmoney NOT NULL,
    CONSTRAINT PK_Ride PRIMARY KEY (RideID),
    CONSTRAINT FK_Ride_MemberID FOREIGN KEY (MemberID)
    REFERENCES Member(MemberID),
    CONSTRAINT FK_Ride_BikeID FOREIGN KEY (BikeID)
    REFERENCES Bike(BikeID),
    CONSTRAINT FK_Ride_PromoCode FOREIGN KEY (PromoCode)
    REFERENCES Promotion(PromoCode),
    CONSTRAINT FK_Ride_BikeStopID FOREIGN KEY (BikeStopID)
    REFERENCES BikeStop(BikeStopID),
    CONSTRAINT CHK_DateTimeEnd CHECK(DateTimeEnd > DateTimeStart)
)