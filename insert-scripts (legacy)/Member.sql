INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'F74DE4481D',
    'Jacob Potts', 
    'hmoreno@example.net',
    '66576653',
    12,
    $2.80,
    $8.90
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'C55E2273AD',
    'Paige Hanson', 
    'yhopkins@example.com',
    '25592794',
    71,
    $1.08,
    $7.89
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '1573EDE77C',
    'Katherine Boyd', 
    'wendy36@example.com',
    '25842773',
    42,
    $7.44,
    $3.51
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '1F4049CE9D',
    'Carlos Boone', 
    'kaylavillarreal@example.org',
    '77216312',
    43,
    $8.05,
    $0.24
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '6216185DF1',
    'Cory Davis', 
    'josephcox@example.org',
    '39457185',
    11,
    $4.86,
    $3.83
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '3DB77544A1',
    'Christopher Smith', 
    'sararamirez@example.net',
    '64674477',
    61,
    $2.92,
    $6.00
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '2931108FC7',
    'Michelle Hill', 
    'emily47@example.com',
    '96666345',
    98,
    $3.94,
    $9.98
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '6DE0DB23AF',
    'April Leach', 
    'johnsonmark@example.org',
    '93627779',
    25,
    $7.02,
    $6.58
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'E3D0358E3A',
    'John Arroyo', 
    'bgarner@example.net',
    '26736168',
    46,
    $5.44,
    $9.29
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'A950C8ECA3',
    'Nicholas Flowers', 
    'moralesjames@example.com',
    '99475848',
    46,
    $6.10,
    $1.54
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '03C0CDC8BB',
    'Ryan Zamora', 
    'jason16@example.net',
    '87353273',
    66,
    $4.26,
    $6.96
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'BF94B72B33',
    'Tracy Mullins', 
    'michael97@example.org',
    '96641937',
    24,
    $1.30,
    $6.62
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '683C51FB12',
    'Justin Edwards', 
    'huntwalter@example.org',
    '95259477',
    32,
    $4.89,
    $0.95
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '82A98C527D',
    'Glenn Brown', 
    'rodriguezdiane@example.com',
    '78514419',
    60,
    $2.20,
    $9.10
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'AAFEDAE3FD',
    'Cynthia Wolf', 
    'gonzalesmarissa@example.net',
    '43512921',
    47,
    $4.94,
    $0.81
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '96BCAEA0E9',
    'Lisa Kelly', 
    'johnnymccoy@example.net',
    '81419173',
    77,
    $1.19,
    $3.04
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '53881BBB8E',
    'Andrew Miller', 
    'robert51@example.com',
    '88328395',
    60,
    $8.22,
    $7.52
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '7BBF52662C',
    'Joshua Jones', 
    'matthewskaren@example.com',
    '16514625',
    90,
    $9.71,
    $4.07
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '050D558ECA',
    'Richard Jackson', 
    'billy48@example.org',
    '39823614',
    39,
    $0.27,
    $8.66
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '8B809A9569',
    'Lindsey Wright', 
    'wdyer@example.org',
    '23564811',
    69,
    $5.67,
    $0.67
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '2F27FC9B58',
    'Angel Fisher', 
    'smithbenjamin@example.com',
    '39328936',
    98,
    $4.22,
    $9.72
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '666BF93C51',
    'Jose Lewis', 
    'guerrerojoseph@example.org',
    '67211627',
    40,
    $3.73,
    $4.75
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'F5747B4012',
    'Andre Tran', 
    'danielsamanda@example.com',
    '48414168',
    80,
    $3.49,
    $4.56
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '8BD3AAE115',
    'Dr. Sarah Macdonald', 
    'joy55@example.com',
    '71951768',
    77,
    $5.47,
    $4.67
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '145A1AF57E',
    'Patrick Howard', 
    'kparrish@example.net',
    '58887951',
    6,
    $1.70,
    $6.23
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '73C399F268',
    'Dylan Jacobs', 
    'justinbowman@example.net',
    '56852182',
    76,
    $8.27,
    $0.34
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'DF787DCD5C',
    'Reginald Jones', 
    'lopezrodney@example.org',
    '84334168',
    55,
    $8.00,
    $3.71
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '4F90C26E51',
    'Kelsey Blevins', 
    'jennifer72@example.org',
    '46594578',
    10,
    $5.01,
    $0.32
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '9D7E170FB3',
    'Jessica Garrett', 
    'jonesheather@example.net',
    '58246863',
    93,
    $4.08,
    $7.50
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'C6B90935D3',
    'Katie Palmer', 
    'richardmatthews@example.org',
    '35698661',
    85,
    $8.79,
    $6.56
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '2889C2FC2F',
    'Thomas Smith', 
    'forozco@example.org',
    '12519922',
    44,
    $9.52,
    $2.63
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '31A7E65391',
    'Tracy Hernandez', 
    'benjamin18@example.com',
    '25218725',
    4,
    $4.46,
    $3.14
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '3D8CE444EF',
    'Francisco Ross', 
    'nicolemitchell@example.net',
    '91915526',
    77,
    $5.74,
    $8.85
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '03DA614150',
    'Anthony Scott', 
    'nelsoncheyenne@example.com',
    '85517246',
    20,
    $5.28,
    $1.13
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'F0470838B3',
    'Jacob Wallace', 
    'paul89@example.com',
    '49866819',
    9,
    $9.96,
    $8.66
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '3C8CAFBC80',
    'Jeffrey Thompson', 
    'bakermatthew@example.org',
    '44931966',
    36,
    $7.12,
    $0.34
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '9192F9D5DD',
    'Casey Duke', 
    'seangolden@example.com',
    '28235818',
    5,
    $9.93,
    $1.35
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '192917A072',
    'Cynthia Petersen', 
    'victor42@example.com',
    '23756347',
    42,
    $5.44,
    $9.37
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '674211395A',
    'Jacob Hart', 
    'chavezwilliam@example.com',
    '94713131',
    49,
    $6.68,
    $7.61
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'AC3714445D',
    'George Barnes', 
    'louis61@example.com',
    '69168478',
    67,
    $8.84,
    $6.54
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'E9DCE9140C',
    'Erica Shelton', 
    'brian33@example.net',
    '83931769',
    19,
    $1.80,
    $0.85
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '79D9D6CE21',
    'Tracy Evans', 
    'qsnyder@example.net',
    '94345992',
    96,
    $6.50,
    $4.21
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '0D5F88FEB1',
    'John Ware', 
    'imartinez@example.net',
    '54928637',
    8,
    $6.47,
    $9.71
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '755904523E',
    'Randall Cunningham', 
    'stephenstyler@example.com',
    '77339836',
    60,
    $4.40,
    $6.44
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'A093764C9F',
    'Amanda Klein', 
    'ljohnson@example.com',
    '65693251',
    98,
    $1.17,
    $7.40
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'EE00A51DBC',
    'Toni Lewis', 
    'mkelly@example.org',
    '93394489',
    5,
    $4.85,
    $7.17
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'DA4D94CB2C',
    'Tracy Jackson', 
    'smithluis@example.net',
    '77664789',
    11,
    $3.94,
    $5.76
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '5CDB62A6D9',
    'Sonia Long', 
    'kevinlutz@example.net',
    '57911866',
    82,
    $2.92,
    $7.54
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '6F7E0E381D',
    'Troy Olsen', 
    'moorejoshua@example.com',
    '52353321',
    6,
    $4.05,
    $9.66
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'D0B17BF34A',
    'Melissa Walter', 
    'sandraramirez@example.net',
    '85677594',
    14,
    $6.90,
    $5.75
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '8D2BC2DC72',
    'Stephanie Baker', 
    'aschultz@example.net',
    '71813132',
    47,
    $2.73,
    $6.02
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '7356425DEB',
    'Becky Bates', 
    'robertshepard@example.org',
    '21174376',
    97,
    $4.40,
    $6.16
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'DE9D191C29',
    'Zachary Vasquez', 
    'lalexander@example.net',
    '65986399',
    85,
    $3.17,
    $3.31
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'FC7974E42B',
    'Deanna Holmes', 
    'gibsonlauren@example.org',
    '94438711',
    60,
    $0.23,
    $6.02
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '50BADDB857',
    'Raymond Roman', 
    'jacqueline62@example.net',
    '77643865',
    95,
    $6.34,
    $2.90
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    '872C21563D',
    'Diana Miranda', 
    'kaufmanclayton@example.org',
    '98552911',
    33,
    $4.58,
    $7.40
)
INSERT INTO Member(MemberID, MemberName, Email, Phone, RewardPts, Deposit, CreditBalance)
VALUES(
    'DAE50B98EA',
    'Ryan Stanton', 
    'reedjoshua@example.org',
    '65872843',
    79,
    $9.84,
    $4.03
)
