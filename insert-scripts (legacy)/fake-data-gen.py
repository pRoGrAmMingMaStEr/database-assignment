# Script to generate T-SQL Scripts (lol??)
from faker import Faker
import random
from datetime import datetime, timedelta
fake = Faker()

def random_from(r, length):
    return "".join(random.choice(r) for i in range(length))

random_id = lambda l: random_from("0123456789ABCDEFabcdef", l)

random_date = lambda: f'\'{fake.date_time_this_year(before_now=False, after_now=True).isoformat()}\''

random_money = lambda x, y: f'${"%0.2f"%random.uniform(x, y)}'

def date_range():
    startdate = fake.date_time_this_year(before_now=True, after_now=False)
    end = fake.date_time_between_dates(datetime_start=startdate, datetime_end=startdate + timedelta(hours=5), tzinfo=None)
    return f'\'{startdate.isoformat()}\'' + ',' + f'\'{end.isoformat()}\''

###
###
## 
#
# Table name here, no spaces
TABLE_NAME = "Fault" 
# Number of tuples
NUMBER_OF_TUPLES = 12
# The Attributes you want to insert here
ATTRIBUTES = "FBID, MaintID, FaultCode"
# The Data fields
FIELDS = lambda: f'''
    \'{random.choice(['A4E32465A9','6866C8CE2A','F139C1A63C','36FD202B87'])}\',
    \'{random.choice(['DB2B1B2926', '0919bE52FF', '0919bE52FF','dFAa75fa0e'])}\',
    \'{random.choice(['dbAdbD0aE9','4CC2fab3bB', 'E28bBE91Bd', '4A4afFf636'])}\'
'''
#
##
###
####

with open(f"{TABLE_NAME}.sql", "w") as sql_dump:
    for i in range(NUMBER_OF_TUPLES):
     sql_dump.write(f'INSERT INTO {TABLE_NAME}({ATTRIBUTES})\n')
     sql_dump.write(f'VALUES({FIELDS()})\n')

