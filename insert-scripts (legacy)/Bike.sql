INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'f3bf0Dc0eD',
    'BF5DF',
    $7.99, 
    'Available'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    '1eCdD1d0fB',
    'CD9A3',
    $4.77, 
    'Available'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'afF42Ae5EE',
    'BF5DF',
    $5.47, 
    'Available'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    '44f5a9a77a',
    'EC0AD',
    $5.44, 
    'Available'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'bfd0Be026f',
    'CD9A3',
    $7.68, 
    'Available'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'ADBa63631B',
    'CD9A3',
    $9.18, 
    'Available'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'D74dDBa69a',
    '76FFA',
    $5.44, 
    'Available'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    '7cAfE6F915',
    'BF5DF',
    $4.49, 
    'Under Repair'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'ECa2F7F753',
    'EC0AD',
    $3.49, 
    'Under Repair'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'bbDe67CEB4',
    '19BA3',
    $1.75, 
    'Under Repair'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'DACe9FcbdC',
    'BF5DF',
    $9.39, 
    'Under Repair'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    '0DAac7afd0',
    'EC0AD',
    $3.92, 
    'Under Repair'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'B29aB90dd6',
    '76FFA',
    $9.85, 
    'Damaged'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'c0e230Ea48',
    '3C8FE',
    $8.67, 
    'Damaged'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    '135d3FeebF',
    'BF5DF',
    $5.82, 
    'Damaged'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'a7A067eEF2',
    'CD9A3',
    $3.95, 
    'Damaged'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'F6A3F0A15C',
    'BF5DF',
    $5.49, 
    'Damaged'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'aAa94B8B4f',
    '76FFA',
    $9.74, 
    'Damaged'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'b7F5FCa933',
    'EC0AD',
    $2.86, 
    'Damaged'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    '0ad9f1AbFD',
    '76FFA',
    $0.69, 
    'Damaged'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'b12080a19e',
    '76FFA',
    $6.48, 
    'Damaged'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    '2D91e40f44',
    '19BA3',
    $1.05, 
    'Damaged'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'd7181e7BdA',
    'BF5DF',
    $7.96, 
    'Damaged'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    '52f4F1CFBe',
    'CD9A3',
    $0.33, 
    'Damaged'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    '7d9a313bCC',
    '3C8FE',
    $0.99, 
    'Damaged'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    '0ddD596417',
    'BF5DF',
    $1.02, 
    'Damaged'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'bE4EbBCDbf',
    '19BA3',
    $6.33, 
    'Under Repair'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    '19CEe56f95',
    '76FFA',
    $4.02, 
    'Under Repair'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'a052aa4C67',
    '19BA3',
    $0.51, 
    'Under Repair'

)
INSERT INTO Bike(BikeID, BikeStopID, Rate, Status)
VALUES(
    'efC03FDB7F',
    '3C8FE',
    $8.54, 
    'Under Repair'

)
