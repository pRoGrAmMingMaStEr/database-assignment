INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '347135605206',
    'Master',
    '2018-01-28T19:28:27',
    '137',
    'F74DE4481D'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '531696274668',
    'Visa',
    '2018-06-10T18:12:41',
    '443',
    'C55E2273AD'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '446138064744',
    'Master',
    '2018-12-04T13:07:38',
    '721',
    '1F4049CE9D'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '384302303104',
    'Visa',
    '2018-05-06T09:16:46',
    '724',
    '1573EDE77C'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '231987628401',
    'Visa',
    '2018-04-18T20:36:55',
    '472',
    '6216185DF1'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '930757416854',
    'Diners',
    '2018-12-24T23:15:49',
    '224',
    '3DB77544A1'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '688169925419',
    'Master',
    '2018-04-21T19:32:55',
    '835',
    '2931108FC7'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '034643204960',
    'Visa',
    '2018-06-21T11:45:40',
    '579',
    '6DE0DB23AF'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '284558549333',
    'Diners',
    '2018-11-28T09:07:31',
    '414',
    'E3D0358E3A'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '428611114293',
    'Master',
    '2018-04-13T03:20:19',
    '396',
    'A950C8ECA3'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '957078622281',
    'Visa',
    '2018-09-11T03:33:45',
    '829',
    'BF94B72B33'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '399224949574',
    'Master',
    '2018-09-13T04:15:21',
    '762',
    'BF94B72B33'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '796716362578',
    'Visa',
    '2018-11-25T01:44:40',
    '944',
    '683C51FB12'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '633046606319',
    'Visa',
    '2018-04-23T01:47:01',
    '395',
    'AAFEDAE3FD'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '636935155263',
    'Diners',
    '2018-02-25T23:08:22',
    '754',
    '96BCAEA0E9'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '704456452098',
    'Master',
    '2018-12-22T03:38:41',
    '186',
    '53881BBB8E'
)
INSERT INTO CreditCard(CardNo, CardType, ExpiryDate, CVV, MemberID)
VALUES(
    '623330657812',
    'Master',
    '2018-03-18T07:07:06',
    '252',
    '7BBF52662C'
)
