-- Creates the database

USE master
CREATE DATABASE MokeBike
GO

USE MokeBike
GO

CREATE TABLE BikeStop
(
    StopLat decimal NOT NULL,
    StopLong decimal NOT NULL,
    StopRadius decimal NOT NULL,
    BikeStopID char(5),
    CONSTRAINT PK_BikeStop PRIMARY KEY (BikeStopID),
    CONSTRAINT CHK_BikeStop_StopRadius CHECK(StopRadius BETWEEN 2 AND 25)
)
GO

CREATE TABLE Bike 
(
    BikeID char(10) NOT NULL,
    BikeStopID char(5) NOT NULL, 
    Rate smallmoney NOT NULL,
    Status varchar(50) NOT NULL,
    CONSTRAINT PK_Bike PRIMARY KEY (BikeID),
    CONSTRAINT FK_Bike_BikeStopID FOREIGN KEY (BikeStopID)
        REFERENCES BikeStop(BikeStopID),
    CONSTRAINT CHK_Bike_Status CHECK (Status in ('Damaged', 'Under Repair', 'Available'))
)
GO

CREATE TABLE Member
( 
    MemberID char(10),
    MemberName varchar(50) NOT NULL,
    Email varchar(100) NOT NULL,
    Phone char(8) NOT NULL,
    RewardPts int NULL,
    Deposit smallmoney DEFAULT 0 NOT NULL,
    CreditBalance smallmoney DEFAULT 0 NOT NULL,
    CONSTRAINT PK_Member_MemberID PRIMARY KEY (MemberID)
)
GO

CREATE TABLE Feedback
(
    FBID char(10) NOT NULL,
    FBDateTime datetime NOT NULL,
    MemberID char(10) NOT NULL,
    CONSTRAINT FK_MemberID FOREIGN KEY (MemberID)
        REFERENCES Member(MemberID),
    CONSTRAINT PK_Feedback PRIMARY KEY(FBID)
)
GO

CREATE TABLE Comment
(
    FBID char(10) NOT NULL,
    Message text NOT NULL,
    CONSTRAINT PK_Comment_FBID PRIMARY KEY (FBID),
    CONSTRAINT FK_Comment_FBID FOREIGN KEY (FBID) 
        REFERENCES Feedback(FBID)
)
GO   

CREATE TABLE CreditCard(
    CardNo char(12) NOT NULL,
    CardType varchar(20) NOT NULL,
    ExpiryDate datetime NOT NULL,
    CVV Char(3) NOT NULL,
    MemberID char(10) NOT NULL,
    CONSTRAINT PK_CreditCard PRIMARY KEY(CardNo),
    CONSTRAINT FK_CreditCard_MemberID
        FOREIGN KEY(MemberID)REFERENCES Member(MemberID),
    CONSTRAINT CHK_CC_ExpiryDate CHECK (ExpiryDate > GETDATE()),
    CONSTRAINT CHK_CC_CardType CHECK (CardType IN ('Visa', 'Master', 'Diners'))
)
GO

CREATE TABLE Promotion
(
    PromoCode char(10),
    StartDate datetime NOT NULL,
    EndDate datetime NOT NULL,
    PromoRate smallmoney NOT NULL,
    PromoDescription varchar(200),
    CONSTRAINT PK_Promotion_PromoCode PRIMARY KEY (PromoCode)
)
GO

CREATE TABLE Ride
(
    RideID char(10) NOT NULL,
    MemberID char(10) NOT NULL,
    BikeID char(10) NOT NULL,
    Origin char(5) NOT NULL,
    Destination char(5) NOT NULL,
    PromoCode char(10) NULL,
    DateTimeStart datetime NOT NULL,
    DateTimeEnd  datetime NOT NULL,
    PtsRedeemed tinyint,
    RidePts tinyint,
    Distance float NOT NULL,
    Cost smallmoney NOT NULL,
    CONSTRAINT PK_Ride PRIMARY KEY (RideID),
    CONSTRAINT FK_Ride_MemberID FOREIGN KEY (MemberID)
    REFERENCES Member(MemberID),
    CONSTRAINT FK_Ride_BikeID FOREIGN KEY (BikeID)
    REFERENCES Bike(BikeID),
    CONSTRAINT FK_Ride_PromoCode FOREIGN KEY (PromoCode)
    REFERENCES Promotion(PromoCode),
    CONSTRAINT FK_Ride_Origin FOREIGN KEY (Origin)
    REFERENCES BikeStop(BikeStopID),
    CONSTRAINT FK_Ride_Destination FOREIGN KEY (Destination)
    REFERENCES BikeStop(BikeStopID),
    CONSTRAINT CHK_DateTimeEnd CHECK(DateTimeEnd > DateTimeStart)
)
GO

CREATE TABLE CreditDebitTrans
(
    TransID char(10) PRIMARY KEY,
    TransAmt smallmoney NOT NULL,
    TransType varchar(10) NOT NULL,
    TransDateTime datetime NOT NULL,
    CardNo char(12) NOT NULL,
    RideID char(10) NOT NULL,
    CONSTRAINT FK_CreditDebitTrans_CardNo
        FOREIGN KEY(CardNo)REFERENCES CreditCard(CardNo),
    CONSTRAINT FK_CreditDebitTrans_RideID
        FOREIGN KEY(RideID)REFERENCES Ride(RideID),
    CONSTRAINT CHK_CreditDebitTrans_TransType
        CHECK (TransType IN ('Debit', 'Top up'))
)
GO

CREATE TABLE FaultType
(
    FaultCode char(10) NOT NULL,
    FaultDesc text NOT NULL,
    CONSTRAINT PK_FaultType PRIMARY KEY (FaultCode)
)
GO

CREATE TABLE Maintenance
(
    MaintID char(10),
    MaintDate datetime NOT NULL,
    BikeID char(10) NOT NULL,
    CONSTRAINT PK_Maintenance_MaintID PRIMARY KEY (MaintID),
    CONSTRAINT FK_Maintenance_BikeID
    FOREIGN KEY (BikeID) REFERENCES Bike(BikeID)
)
GO

CREATE TABLE Fault
(
    FBID char(10) NOT NULL,
    MaintID char(10) NOT NULL,
    FaultCode char(10) NOT NULL,
    BikeID char(10) NOT NULL,
    CONSTRAINT FK_Fault_BikeID FOREIGN KEY (BikeID) 
        REFERENCES Bike(BikeID),
    CONSTRAINT FK_Fault_FBID 
    FOREIGN KEY (FBID) REFERENCES Feedback(FBID),
    CONSTRAINT FK_Fault_FaultType 
    FOREIGN KEY (FaultCode) REFERENCES FaultType(FaultCode),
    CONSTRAINT FK_Fault_MaintID
    FOREIGN KEY (MaintID) REFERENCES Maintenance(MaintID)
)
GO

CREATE TABLE MaintType 
(
    TypeCode char(10),
    TypeDesc char(100) NOT NULL,
    CONSTRAINT PK_MaintType_TypeCode PRIMARY KEY (TypeCode)
)
GO

CREATE TABLE MaintItem
(
  MaintID char(10) NOT NULL,
  SNo char(10) NOT NULL,
  TypeCode char(10) NOT NULL,
  Desc_ text,
  Constraint PK_MaintItem PRIMARY KEY(MaintID, SNo),
  Constraint FK_MaintItem_TypeCode FOREIGN KEY(TypeCode)
    REFERENCES MaintType(TypeCode),
  Constraint FK_MaintItem_MaintID FOREIGN KEY(MaintID)
    REFERENCES Maintenance(MaintID)
)
GO


CREATE TABLE Repair
(
    MaintID char(10) NOT NULL,
    CONSTRAINT PK_Repair_MaintID PRIMARY KEY (MaintID),
    CONSTRAINT FK_Repair_MaintID FOREIGN KEY (MaintID) 
        REFERENCES Maintenance(MaintID)
)
GO

CREATE TABLE Reservation
(
  MemberID char(10) NOT NULL ,
  BikeID char(10) NOT NULL,
  ResStatus varchar(50) NOT NULL,
  DateTimeRes datetime NOT NULL,
  DateTimeReservedFor datetime NOT NULL,
  CONSTRAINT PK_Reservation PRIMARY KEY (MemberID,BikeID),
  CONSTRAINT FK_Reservation_MemberID 
	FOREIGN KEY (MemberID) REFERENCES Member(MemberID),
  CONSTRAINT FK_Reservation_BikeID 
	FOREIGN KEY (BikeID) REFERENCES Bike(BikeID),
  CONSTRAINT CHK_ResStatus CHECK(ResStatus IN ('Confirmed', 'Cancelled', 'NoShow')),
  CONSTRAINT CHK_Reservation_DateTime CHECK(
    DATEDIFF(hour, DateTimeRes, DateTimeReservedFor) <= 2 
    AND DateTimeRes < DateTimeReservedFor)
) 
GO