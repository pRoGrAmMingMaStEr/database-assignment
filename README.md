# Database Assignment SQL Scripts

# Usage
Run `build.sql`, then `insert.sql` to start working with the Database.
Run `destory.sql` to remove the database.

## NOTE: Finalised version
As of the latest merge, all hot fixes are to be done to `build.sql` and `insert.sql`, individual sql scripts in the workspaces are all outdated

## Database Creation Scripts

For **_each_** table/relation you are making, create a **new** SQL file to prevent merge conflicts, we'll merge everything back into 1 file at the end of the week.
For the relation `ride`, we'll have `ride.sql`

## Individual Query Scripts
If you wish to use this repo to work on your own individual statements, feel free to do so by creating a folder for your own scripts.